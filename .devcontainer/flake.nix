{
    description = "devcontainer flake";

    inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    };

    outputs = { self, nixpkgs } @ inputs:
    let
        flakeContext = {
            inherit inputs;
        };
        system = "x86_64-linux";
        pkgs = nixpkgs.legacyPackages.${system};
        cfg = pkgs.writeTextDir "sshd_config" ''
            # Change to yes to enable challenge-response passwords (beware issues with
            # some PAM modules and threads)
            KbdInteractiveAuthentication no

            # Set this to 'yes' to enable PAM authentication, account processing,
            # and session processing. If this is enabled, PAM authentication will
            # be allowed through the KbdInteractiveAuthentication and
            # PasswordAuthentication.  Depending on your PAM configuration,
            # PAM authentication via KbdInteractiveAuthentication may bypass
            # the setting of "PermitRootLogin without-password".
            # If you just want the PAM account and session checks to run without
            # PAM authentication, then enable this but set PasswordAuthentication
            # and KbdInteractiveAuthentication to 'no'.
            UsePAM yes

            # Allow client to pass locale environment variables
            AcceptEnv LANG LC_*
        '';
        openssh = pkgs.openssh;
        nixosModules = {
            docker = import ./nixosModules/docker.nix flakeContext;
            services = import ./nixosModules/services.nix flakeContext;
            user-root = import ./nixosModules/user-root.nix flakeContext;
        };
        sshdUser = pkgs.writeText "passwd-tail"
        ''
          sshd:x:40009:40000:sshd:/var/empty:/bin/false
        '';
    in {
        packages = {
            x86_64-linux = {
                user = pkgs.concatTextFile {
                    name = "sshd-user";
                    files =  ["/etc/passwd" sshdUser];
                    destination = "/etc/passwd";
                };

                default = pkgs.writeShellApplication {
                    name = "init";

                    runtimeInputs = [ openssh ];

                    text = ''
                        ${openssh}/bin/sshd -D -f ${cfg}/sshd_config
                    '';
                };
            };
        };
    };
}
