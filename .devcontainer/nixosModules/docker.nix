{ inputs, ... }@flakeContext:
{ config, lib, pkgs, ... }: {
  config = {
    boot = {
      isContainer = true;
      loader = {
        initScript = {
          enable = true;
        };
      };
      tmp = {
        useTmpfs = true;
      };
    };
    networking = {
      firewall = {
        enable = false;
      };
      useDHCP = false;
      useHostResolvConf = false;
      useNetworkd = true;
    };
  };
}
