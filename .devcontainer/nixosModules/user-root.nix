{ inputs, ... }@flakeContext:
{ config, lib, pkgs, ... }: {
  config = {
    users = {
      users = {
        root = {
          openssh = {
            authorizedKeys = {
              keys = [
                "ssh-rsa asdf foo@bar"
              ];
            };
          };
          packages = [
            pkgs.git
          ];
          password = "root";
        };
      };
    };
  };
}
