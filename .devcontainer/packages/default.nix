{ inputs, ... }@flakeContext:
let
  nixosModule = { config, lib, pkgs, ... }: {
    imports = [
      inputs.self.nixosModules.docker
      inputs.self.nixosModules.services
      inputs.self.nixosModules.user-root
    ];
  };
in
inputs.nixos-generators.nixosGenerate {
  system = "x86_64-linux";
  format = "docker";
  modules = [
    nixosModule
  ];
}
